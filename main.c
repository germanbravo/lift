#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <time.h>
#include <signal.h>
#include "fsm.h"

#define T_abrir_puertas 2000    // tiempo mientras se abren las puertas
#define T_puertas_abiertas 4000 // tiempo de puertas abiertas

//#define DEBUG 1

/*
 * VARIABLES
*/ 
struct Peticiones {
  unsigned int exterior;   // variable para las peticiones del exterior del ascensor
  unsigned int cabina : 1; // variable para las peticiones de cabina
  unsigned int guardada;   // variables para las peticiones pendientes
} peticion;

static int ascensor_movimiento = 0; // aviso de ascensor en movimiento
static int destino = 0;  // variable para introducir la planta a la que se desea ir

/*
 * ENTRADAS
*/ 
static int exterior = 0; // entrada del boton del exterior
static int cabina = 0;   // entrada del boton de cabina
static int sensor = 0;   // entrada del sensor de posicion

/*
 * Variables y funciones de tiempo 
*/
static int timer = 0;
static void timer_isr (union sigval arg) { timer = 1; }
static void timer_start (int ms)
{
  timer_t timerid;
  struct itimerspec spec;
  struct sigevent se;
  se.sigev_notify = SIGEV_THREAD;
  se.sigev_value.sival_ptr = &timerid;
  se.sigev_notify_function = timer_isr;
  se.sigev_notify_attributes = NULL;
  spec.it_value.tv_sec = ms / 1000;
  spec.it_value.tv_nsec = (ms % 1000) * 1000000;
  spec.it_interval.tv_sec = 0;
  spec.it_interval.tv_nsec = 0;
  timer_create (CLOCK_REALTIME, &se, &timerid);
  timer_settime (timerid, 0, &spec, NULL);
}

static int timer_finished (fsm_t* this) { return timer; }



/**********************************************************************
			    Línea_1_semp_ascensor                               
**********************************************************************/
static int evaluar_destino_menor (fsm_t* this)
{
  if (sensor > destino) {
    ascensor_movimiento = 1;
	return 1;
  }
  else return 0;
}

static void bajar (fsm_t* this)
{
  printf("\nAscensor bajando... \n");
  printf("\nEl ascensor se encuentra en la planta: %d, se dirige a la planta: %d \n", sensor, destino);
}

/**********************************************************************
			    Línea_2_semp_ascensor                               
**********************************************************************/
static int evaluar_destino_mayor (fsm_t* this)
{
  if (sensor < destino) {
    ascensor_movimiento = 1;
	return 1;
  }
  else return 0;
}

static void subir (fsm_t* this)
{
  printf("\nAscensor subiendo... \n");
  printf("\nEl ascensor se encuentra en la planta: %d, se dirige a la planta: %d \n", sensor, destino);
}

/**********************************************************************
			    Línea_3_semp_ascensor
**********************************************************************/
static int evaluar_abrir (fsm_t* this)
{
  if ((sensor == destino && sensor == exterior && peticion.exterior == 1) || 
      (sensor == destino && sensor == cabina && peticion.cabina == 1)) 
  {
	timer = 0;
    timer_start(T_puertas_abiertas);
	return 1;
  }
  else return 0;
}

static void abrirpuertas (fsm_t* this)
{
  printf("\nAbriendo puertas... \n");
  printf("\nMarque nuevo destino o espere a que se cierren las puertas\n");
}

/**********************************************************************
			    Línea_4y5_semp_ascensor                            
**********************************************************************/
static int evaluar_destino_igual (fsm_t* this)
{
  if (sensor == destino) {
    ascensor_movimiento = 0;
    timer = 0;
    timer_start(T_abrir_puertas);
	return 1;
  }
  else return 0;
}

static void parar (fsm_t* this)
{
  printf("\nAscensor parando... \n");
  printf("\nEl ascensor ha llegado a su destino, planta: %d \n", sensor);
  printf("\nEspere a que se abran las puertas\n");
}

/**********************************************************************
			    Línea_8_semp_ascensor                               
**********************************************************************/
static int evaluar_destino_diff (fsm_t* this)
{
  if (sensor != destino) {
    return 1;
  }
  else return 0;
}

static void cerrarpuertas (fsm_t* this)
{
  printf("\nCerrando puertas... \n");
}

/*********************************************************************/

// Explicit Ascensor FSM states and description 
enum ascensor_state {
  PUERTAS_CERRADAS,
  PUERTAS_ABIERTAS,
  PARADO,
  BAJAR,
  SUBIR,
};

static fsm_trans_t semp_ascensor[] = {
/*Estado_actual      condición_de_guarda       estado_siguiente   acción */
{ PUERTAS_CERRADAS,  evaluar_destino_menor,    BAJAR,             bajar },
{ PUERTAS_CERRADAS,  evaluar_destino_mayor,    SUBIR,             subir },
{ PUERTAS_CERRADAS,  evaluar_abrir,            PUERTAS_ABIERTAS,  abrirpuertas },
{ BAJAR,             evaluar_destino_igual,    PARADO,            parar },
{ SUBIR,             evaluar_destino_igual,    PARADO,            parar },
{ PARADO,            timer_finished,           PUERTAS_ABIERTAS,  abrirpuertas },
{ PUERTAS_ABIERTAS,  timer_finished,           PUERTAS_CERRADAS,  cerrarpuertas },
{ PUERTAS_ABIERTAS,  evaluar_destino_diff,     PUERTAS_CERRADAS,  cerrarpuertas },
{-1,                 NULL,                     -1,                NULL },
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**********************************************************************
			    Línea_1_semp_pet                            
**********************************************************************/
static int llamada_cabina (fsm_t* this)
{
  if (ascensor_movimiento == 0 && peticion.cabina == 1) {
    return 1;
  }
  else return 0;
}

static void pet_cabina (fsm_t* this)
{
  peticion.cabina = 0;
  destino = cabina;
}

/**********************************************************************
			    Línea_2_semp_pet                               
**********************************************************************/
static int llamada_exterior (fsm_t* this)
{
  if (ascensor_movimiento == 0 && peticion.exterior == 1) {
    return 1;
  }
  else return 0;
}

static void pet_exterior (fsm_t* this)
{
  peticion.exterior = 0;
  destino = exterior;
}

/**********************************************************************
			    Línea_3_semp_pet                               
**********************************************************************/
static int llamada_cabina_mov (fsm_t* this) 
{
  if (peticion.cabina == 1 && ascensor_movimiento == 1) {
	return 1;
  }
  else return 0;
}

static void guardar_pet_cabina (fsm_t* this)
{
  printf("\nPeticion cabina guardada \n");
  peticion.cabina = 0;
  peticion.guardada = cabina;
}

/**********************************************************************
			    Línea_4_semp_pet                               
**********************************************************************/
static int llamada_exterior_mov (fsm_t* this) 
{
  if (peticion.exterior == 1 && ascensor_movimiento == 1) {
    return 1;
  }
  else return 0;
}

static void guardar_pet_exterior (fsm_t* this)
{
  printf("\nPeticion exterior guardada \n");
  peticion.exterior = 0;
  peticion.guardada = exterior;
}

/**********************************************************************
			    Línea_5_semp_pet                               
**********************************************************************/
static int ascensor_parado (fsm_t* this) 
{
  if (ascensor_movimiento == 0) {
    return 1;
  }
  else return 0;
}

static void realizar_pet_guardada (fsm_t* this)
{
  destino = peticion.guardada;
}

/*********************************************************************/

// Explicit Peticiones FSM states and description 
enum pet_state {
  NO_PETICIONES,
  PETICIONES,
};

static fsm_trans_t semp_pet[] = {
/*Estado_actual   condición_de_guarda    estado_siguiente   acción */
{ NO_PETICIONES,  llamada_cabina,        NO_PETICIONES,     pet_cabina },
{ NO_PETICIONES,  llamada_exterior,      NO_PETICIONES,     pet_exterior },
{ NO_PETICIONES,  llamada_cabina_mov,    PETICIONES,        guardar_pet_cabina },
{ NO_PETICIONES,  llamada_exterior_mov,  PETICIONES,        guardar_pet_exterior },
{ PETICIONES,     ascensor_parado,       NO_PETICIONES,     realizar_pet_guardada },
{ PETICIONES,     llamada_cabina_mov,    PETICIONES,        guardar_pet_cabina },
{ PETICIONES,     llamada_exterior_mov,  PETICIONES,        guardar_pet_exterior },
{-1,              NULL,                  -1,                NULL },
};


// Funcion para mostrar información actualizada
static void informacion_actualizacion(fsm_t* as_this, fsm_t* pet_this) {
  switch (as_this -> current_state) {
    case 0: //PUERTAS_CERRADAS
	   printf("\nPuertas cerradas\n");
	   break;
	case 1 : //PUERTAS_ABIERTAS
       printf("\nMarque nuevo destino o espere a que se cierren las puertas\n");
	   break;
    case 2 : //PARADO
       printf("\nEl ascensor ha llegado a su destino, planta: %d \n", sensor);
       printf("Espere a que se abran las puertas\n");
	   break;
    case 3 : //BAJAR
       printf("\nAscensor bajando... \n");
       printf("\nEl ascensor se encuentra en la planta: %d, se dirige a la planta: %d \n", sensor, destino);
	   switch (pet_this -> current_state) {  
	      case 0: //NO_PETICIONES
	         printf("Sin peticiones guardadas\n");
             break;
		  case 1: // PETICIONES
		     printf("Hay una peticion guardada a la planta: %d\n", peticion.guardada);
			 break;
	   }
	   break;
	case 4 : //SUBIR
       printf("\nAscensor subiendo... \n");
       printf("\nEl ascensor se encuentra en la planta: %d, se dirige a la planta: %d \n", sensor, destino);
	   switch (pet_this -> current_state) {  
	      case 0: //NO_PETICIONES
	         printf("Sin peticiones guardadas\n");
             break;
		  case 1: // PETICIONES
		     printf("Hay una peticion guardada a la planta: %d\n", peticion.guardada);
			 break;
	   }
	   break;
  }
}

// Funcion para leer el teclado y enviarle órdenes a la FSM
static void realizar_lectura(fsm_t* as_this, fsm_t* pet_this) {
  static int lectura = 5; // inicializacion a otro valor que no se utiliza
  scanf("%d", &lectura);
  switch (lectura) {
    case 0:
	   exterior = 0;
	   peticion.exterior = 1;
	   printf("\nHa pulsado el boton exterior: %d\n", exterior);
       break;
    case 1:
       exterior = 1;
	   peticion.exterior = 1;
	   printf("\nHa pulsado el boton exterior: %d\n", exterior);
       break;
	case 2:
	   exterior = 2;
	   peticion.exterior = 1;
	   printf("\nHa pulsado el boton exterior: %d\n", exterior);
	   break;
	case 3:
       informacion_actualizacion(as_this, pet_this);
	   break;
	case 10:
	   cabina = 0;
	   peticion.cabina = 1;
	   printf("\nHa pulsado el boton de cabina: %d\n", cabina);
	   break;
	case 11:
	   cabina = 1;
	   peticion.cabina = 1;
	   printf("\nHa pulsado el boton de cabina: %d\n", cabina);
	   break;
	case 12:
	   cabina = 2;
	   peticion.cabina = 1;
	   printf("\nHa pulsado el boton de cabina: %d\n", cabina);
	   break;
	case 100:
	   sensor = 0;
	   printf("\nAscensor en planta: %d \n", sensor);
	   break;
	case 101:
	   sensor = 1;
	   printf("\nAscensor en planta: %d \n", sensor);
	   break;
	case 102:
	   sensor = 2;
	   printf("\nAscensor en planta: %d \n", sensor);
	   break;
    default:
	   printf ("\nEntrada inservible, actualizando FSM... \n");
       break;
  }
}



// Utility functions, should be elsewhere

// res = a - b
void timeval_sub (struct timeval *res, struct timeval *a, struct timeval *b)
{
  res->tv_sec = a->tv_sec - b->tv_sec;
  res->tv_usec = a->tv_usec - b->tv_usec;
  if (res->tv_usec < 0) {
    --res->tv_sec;
    res->tv_usec += 1000000;
  }
}

// res = a + b
void timeval_add (struct timeval *res, struct timeval *a, struct timeval *b)
{
  res->tv_sec = a->tv_sec + b->tv_sec + a->tv_usec / 1000000 + b->tv_usec / 1000000; 
  res->tv_usec = a->tv_usec % 1000000 + b->tv_usec % 1000000;
}

// wait until next_activation (absolute time)
void delay_until (struct timeval* next_activation)
{
  struct timeval now, timeout;
  gettimeofday (&now, NULL);
  timeval_sub (&timeout, next_activation, &now);
  select (0, NULL, NULL, NULL, &timeout);
}

// Mostrar el current_state por su nombre
#ifdef DEBUG
  static void estado_actual (fsm_t* this) 
  {
    switch (this->current_state) {
       case 0 :
          printf("PUERTAS_CERRADAS\n");
	      break;
       case 1 :
          printf("PUERTAS_ABIERTAS\n");
          break;
       case 2 :
          printf("PARADO\n");
          break;
       case 3 :
          printf("BAJAR\n");
          break;
	   case 4 :
	      printf("SUBIR\n");
		  break;
    }
  }
  static void estado_actual2 (fsm_t* this) 
  {
    switch (this->current_state) {
       case 0 :
          printf("NO_PETICIONES\n");
	      break;
       case 1 :
          printf("PETICIONES\n");
          break;
    }  
  }
  static void valores_variables()
  {
    printf("\nVARIABLES: \n peticion.exterior = %d \n peticion.cabina = %d \n", peticion.exterior, peticion.cabina);
    printf(" peticion.guardada = %d \n ascensor_movimiento = %d \n destino = %d \n", peticion.guardada, ascensor_movimiento, destino);
    printf("ENTRADAS: \n exterior = %d \n cabina = %d \n sensor = %d \n\n", exterior, cabina, sensor);
  }
#endif


int main ()
{
  struct timeval clk_period = { 0, 250 * 1000 }; // Maquina de estados activada por tiempo
  struct timeval next_activation;
  
  fsm_t* fsm_peticiones = fsm_new (semp_pet);
  fsm_t* fsm_ascensor = fsm_new (semp_ascensor);

  printf("\nEl ascensor se encuentra en la planta 0, con las puertas cerradas.\n");
  gettimeofday (&next_activation, NULL);
  while (1) {
    #ifdef DEBUG
	  printf("\nEstado actual fsm_ascensor: ");
      estado_actual(fsm_ascensor); // funcion de print del current_state fsm_ascensor 
	  printf("\nEstado actual fsm_peticiones: ");
      estado_actual2(fsm_peticiones); // funcion de print del current_state fsm_ascensor 
	  valores_variables();
	#endif
	
	realizar_lectura(fsm_ascensor, fsm_peticiones);
	
	fsm_fire (fsm_ascensor);
	fsm_fire (fsm_peticiones);
	
    gettimeofday (&next_activation, NULL);
    timeval_add (&next_activation, &next_activation, &clk_period);
    delay_until (&next_activation);
  }
	
  return 1;
}
